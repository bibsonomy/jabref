/* (C) 2003 Nizar N. Batada, Morten O. Alver

 All programs in this directory and
 subdirectories are published under the GNU General Public License as
 described below.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA

 Further information about the GNU GPL is available at:
 http://www.gnu.org/copyleft/gpl.ja.html

 */
package net.sf.jabref;

import java.awt.FileDialog;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.MissingResourceException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Filter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import net.sf.jabref.collab.FileUpdateMonitor;
import net.sf.jabref.imports.ImportFormatReader;
import net.sf.jabref.journals.JournalAbbreviations;
import net.sf.jabref.util.ErrorConsole;

public class Globals extends GlobalsSuper {

	public static FileUpdateMonitor fileUpdateMonitor = new FileUpdateMonitor();

	public static ImportFormatReader importFormatReader = new ImportFormatReader();

	public static ErrorConsole errorConsole;
	
	static {
		// TODO: Error console initialization here. When should it be used?
		errorConsole = ErrorConsole.getInstance();
	}

	private static Handler consoleHandler = new java.util.logging.ConsoleHandler();

	public static GlobalFocusListener focusListener = new GlobalFocusListener();
    
	public static HelpDialog helpDiag = null;

	public static SidePaneManager sidePaneManager;

	public static void turnOffLogging() { // only log exceptions
		logger.setLevel(java.util.logging.Level.SEVERE);
	}

	/**
	 * Should be only called once
	 */
	public static void turnOnConsoleLogging() {
		logger.addHandler(consoleHandler);
	}

	/**
	 * Should be only called once
	 */
	public static void turnOnFileLogging() {
		logger.setLevel(java.util.logging.Level.ALL);
		java.util.logging.Handler handler;
		handler = new ConsoleHandler();
		logger.addHandler(handler);

		handler.setFilter(new Filter() { // select what gets logged
				public boolean isLoggable(LogRecord record) {
					return true;
				}
			});
	}

	public static void setLanguage(String language, String country) {
		GlobalsSuper.setLanguage(language, country);
		javax.swing.JComponent.setDefaultLocale(locale);
	}

	public static JournalAbbreviations journalAbbrev;


	public static String menuTitle(String key) {
		String translation = null;
		try {
			if (Globals.messages != null) {
				translation = Globals.menuTitles.getString(key.replaceAll(" ", "_"));
			}
		} catch (MissingResourceException ex) {
			translation = key;
		}
		if ((translation != null) && (translation.length() != 0)) {
			return translation.replaceAll("_", " ");
		} else {
			return key;
		}
	}

	public static String getIntegrityMessage(String key) {
		String translation = null;
		try {
			if (Globals.intMessages != null) {
				translation = Globals.intMessages.getString(key);
			}
		} catch (MissingResourceException ex) {
			translation = key;

			// System.err.println("Warning: could not get menu item translation
			// for \""
			// + key + "\"");
		}
		if ((translation != null) && (translation.length() != 0)) {
			return translation;
		} else {
			return key;
		}
	}

	// ============================================================
	// Using the hashmap of entry types found in BibtexEntryType
	// ============================================================
	public static BibtexEntryType getEntryType(String type) {
		// decide which entryType object to return
		Object o = BibtexEntryType.ALL_TYPES.get(type);
		if (o != null) {
			return (BibtexEntryType) o;
		} else {
			return BibtexEntryType.OTHER;
		}
		/*
		 * if(type.equals("article")) return BibtexEntryType.ARTICLE; else
		 * if(type.equals("book")) return BibtexEntryType.BOOK; else
		 * if(type.equals("inproceedings")) return
		 * BibtexEntryType.INPROCEEDINGS;
		 */
	}

	/**
	 * Will return the names of multiple files selected in the given directory
	 * and the given extensions.
	 * 
	 * Will return an empty String array if no entry is found.
	 * 
	 * @param owner
	 * @param directory
	 * @param extension
	 * @param updateWorkingdirectory
	 * @return an array of selected file paths, or an empty array if no selection is made.
	 */
	public static String[] getMultipleFiles(JFrame owner, File directory, String extension,
		boolean updateWorkingdirectory) {

		OpenFileFilter off = null;
		if (extension == null)
			off = new OpenFileFilter();
		else if (!extension.equals(NONE))
			off = new OpenFileFilter(extension);

		Object files = getNewFileImpl(owner, directory, extension, null, off,
			JFileChooser.OPEN_DIALOG, updateWorkingdirectory, false, true, null);

		if (files instanceof String[]) {
			return (String[]) files;
		}
		// Fix for:
		// http://sourceforge.net/tracker/index.php?func=detail&aid=1538769&group_id=92314&atid=600306
		if (files != null) {
			return new String[] { (String) files };
		}
		return new String[0];
	}

	public static String getNewFile(JFrame owner, File directory, String extension, int dialogType,
		boolean updateWorkingDirectory) {
		return getNewFile(owner, directory, extension, null, dialogType, updateWorkingDirectory,
			false, null);
	}

        public static String getNewFile(JFrame owner, File directory, String extension, int dialogType,
		boolean updateWorkingDirectory, JComponent accessory) {
		return getNewFile(owner, directory, extension, null, dialogType, updateWorkingDirectory,
			false, accessory);
	}

        
	public static String getNewFile(JFrame owner, File directory, String extension,
		String description, int dialogType, boolean updateWorkingDirectory) {
		return getNewFile(owner, directory, extension, description, dialogType,
			updateWorkingDirectory, false, null);
	}

	public static String getNewDir(JFrame owner, File directory, String extension, int dialogType,
		boolean updateWorkingDirectory) {
		return getNewFile(owner, directory, extension, null, dialogType, updateWorkingDirectory,
			true, null);
	}

	public static String getNewDir(JFrame owner, File directory, String extension,
		String description, int dialogType, boolean updateWorkingDirectory) {
		return getNewFile(owner, directory, extension, description, dialogType,
			updateWorkingDirectory, true, null);
	}

	private static String getNewFile(JFrame owner, File directory, String extension,
		String description, int dialogType, boolean updateWorkingDirectory, boolean dirOnly,
                JComponent accessory) {

		OpenFileFilter off = null;

		if (extension == null)
			off = new OpenFileFilter();
		else if (!extension.equals(NONE))
			off = new OpenFileFilter(extension);

		return (String) getNewFileImpl(owner, directory, extension, description, off, dialogType,
			updateWorkingDirectory, dirOnly, false, accessory);
	}

	private static Object getNewFileImpl(JFrame owner, File directory, String extension,
		String description, OpenFileFilter off, int dialogType, boolean updateWorkingDirectory,
		boolean dirOnly, boolean multipleSelection, JComponent accessory) {

        // Added the !dirOnly condition below as a workaround to the native file dialog
        // not supporting directory selection:
        if (!dirOnly && prefs.getBoolean("useNativeFileDialogOnMac")) {

			return getNewFileForMac(owner, directory, extension, dialogType,
				updateWorkingDirectory, dirOnly, off);
		}

		JFileChooser fc;
		try {
			fc = new JFileChooser(directory);//JabRefFileChooser(directory);
            if (accessory != null)
                fc.setAccessory(accessory);
		} catch (InternalError errl) {
			// This try/catch clause was added because a user reported an
			// InternalError getting thrown on WinNT, presumably because of a
			// bug in JGoodies Windows PLAF. This clause can be removed if the
			// bug is fixed, but for now we just resort to the native file
			// dialog, using the same method as is always used on Mac:
			return getNewFileForMac(owner, directory, extension, dialogType,
				updateWorkingDirectory, dirOnly, off);
		}

		if (dirOnly) {
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		}

		fc.setMultiSelectionEnabled(multipleSelection);

		fc.addChoosableFileFilter(off);
		fc.setDialogType(dialogType);
		int dialogResult;
		if (dialogType == JFileChooser.OPEN_DIALOG) {
			dialogResult = fc.showOpenDialog(owner);
		} else if (dialogType == JFileChooser.SAVE_DIALOG) {
			dialogResult = fc.showSaveDialog(owner);
		} else {
			dialogResult = fc.showDialog(owner, description);
		}

		// the getSelectedFile method returns a valid fileselection
		// (if something is selected) indepentently from dialog return status
		if (dialogResult != JFileChooser.APPROVE_OPTION)
			return null;

		// okay button
		File selectedFile = fc.getSelectedFile();
		if (selectedFile == null) { // cancel
			return null;
		}

		// If this is a save dialog, and the user has not chosen "All files" as
		// filter
		// we enforce the given extension. But only if extension is not null.
		if ((extension != null) && (dialogType == JFileChooser.SAVE_DIALOG)
			&& (fc.getFileFilter() == off) && !off.accept(selectedFile)) {

			// add the first extension if there are multiple extensions
			selectedFile = new File(selectedFile.getPath() + extension.split("[, ]+", 0)[0]);
		}

		if (updateWorkingDirectory) {
			prefs.put("workingDirectory", selectedFile.getPath());
		}

		if (!multipleSelection)
			return selectedFile.getAbsolutePath();
		else {
			File[] files = fc.getSelectedFiles();
			String[] filenames = new String[files.length];
			for (int i = 0; i < files.length; i++)
				filenames[i] = files[i].getAbsolutePath();
			return filenames;
		}
	}

	private static String getNewFileForMac(JFrame owner, File directory, String extensions,
		int dialogType, boolean updateWorkingDirectory, boolean dirOnly, FilenameFilter filter) {

		FileDialog fc = new FileDialog(owner);

		// fc.setFilenameFilter(filter);
		if (directory != null) {
			fc.setDirectory(directory.getParent());
		}
		if (dialogType == JFileChooser.OPEN_DIALOG) {
			fc.setMode(FileDialog.LOAD);
		} else {
			fc.setMode(FileDialog.SAVE);
		}

		fc.setVisible(true); // fc.show(); -> deprecated since 1.5
        
		if (fc.getFile() != null) {
			Globals.prefs.put("workingDirectory", fc.getDirectory() + fc.getFile());
			return fc.getDirectory() + fc.getFile();
		} else {
			return null;
		}
	}

	static {

		// System.out.println(journalAbbrev.getAbbreviatedName("Journal of Fish
		// Biology", true));
		// System.out.println(journalAbbrev.getAbbreviatedName("Journal of Fish
		// Biology", false));
		// System.out.println(journalAbbrev.getFullName("Aquaculture Eng."));
		/*
		 * for (Iterator i=journalAbbrev.fullNameIterator(); i.hasNext();) {
		 * String s = (String)i.next();
		 * System.out.println(journalAbbrev.getFullName(s)+" :
		 * "+journalAbbrev.getAbbreviatedName(s, true)); }
		 */

		// Start the thread that monitors file time stamps.
		// Util.pr("Starting FileUpdateMonitor thread. Globals line 293.");
		fileUpdateMonitor.start();

		try {
			SHORTCUT_MASK = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
		} catch (Throwable t) {

		}
	}
	

	public static void initializeJournalNames() {
		if (prefs.getBoolean("useIEEEAbrv"))
			journalAbbrev = new JournalAbbreviations("/resource/IEEEJournalList.txt");
        else
            journalAbbrev = new JournalAbbreviations();

		// Read external lists, if any (in reverse order, so the upper lists
		// override the lower):
		String[] lists = prefs.getStringArray("externalJournalLists");
		if ((lists != null) && (lists.length > 0)) {
			for (int i = lists.length - 1; i >= 0; i--) {
				try {
					journalAbbrev.readJournalList(new File(lists[i]));
				} catch (FileNotFoundException e) {
					// The file couldn't be found... should we tell anyone?
					Globals.logger(e.getMessage());
				}
			}
		}

		// Read personal list, if set up:
		if (prefs.get("personalJournalList") != null) {
			try {
				journalAbbrev.readJournalList(new File(prefs.get("personalJournalList")));
			} catch (FileNotFoundException e) {
				Globals.logger("Personal journal list file '" + prefs.get("personalJournalList")
					+ "' not found.");
			}
		}

	}

}
